/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   util.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/25 14:23:15 by cumberto          #+#    #+#             */
/*   Updated: 2017/09/25 15:35:16 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

void				update_data(t_data *elem)
{
	elem->max = g_data.max;
	elem->win[ROW] = g_data.win[ROW];
	elem->win[COL] = g_data.win[COL];
	elem->lcol = g_data.lcol;
	elem->curr = g_data.curr;
}

void				free_list(t_slct *elem, unsigned int tot)
{
	unsigned int	cnt;
	t_slct			*tmp;

	cnt = 0;
	while (cnt < tot)
	{
		tmp = elem;
		elem = elem->next;
		free(tmp->name);
		free(tmp);
		cnt++;
	}
}

void				update_ind(t_slct *elem, int tot)
{
	int				cnt;

	cnt = 0;
	tot = g_data.tot - elem->ind + 1;
	while (cnt < tot)
	{
		elem->ind = elem->ind - 1;
		elem = elem->next;
		cnt++;
	}
}

int					cnt_size(t_data *elem, int buf)
{
	t_slct			*tmp;
	int				num;

	num = elem->win[ROW];
	tmp = elem->curr;
	if (elem->lcol != elem->tot)
	{
		if (buf == RIGHT && elem->tot - tmp->ind < elem->win[ROW])
		{
			num = elem->tot - tmp->ind;
			if (tmp->ind > elem->lcol)
				num += tmp->pos[ROW] + 1;
		}
		if (buf == LEFT && tmp->ind <= elem->win[ROW])
		{
			num = elem->win[ROW] - (elem->win[ROW] - (elem->tot - elem->lcol));
			if (tmp->ind > num)
				num += tmp->ind - num;
		}
	}
	return (num);
}

void				fill_data(t_data *elem)
{
	struct winsize	win;
	int				max;

	max = len_finder(elem->list, elem->tot) + 2;
	ioctl(0, TIOCGWINSZ, &win);
	elem->win[ROW] = win.ws_row;
	elem->win[COL] = win.ws_col;
	elem->max = max;
	elem->lcol = elem->tot - (elem->tot % win.ws_row);
}
