/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/15 15:20:16 by cumberto          #+#    #+#             */
/*   Updated: 2017/09/25 16:30:24 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

static char			*ft_strdup_const(const char *str)
{
	int				len;
	char			*copy;
	unsigned int	cnt;

	cnt = 0;
	len = ft_strlen(str);
	copy = (char *)malloc(sizeof(char) * len + 1);
	while (str[cnt])
	{
		copy[cnt] = str[cnt];
		cnt++;
	}
	copy[cnt] = '\0';
	return (copy);
}

t_slct				*make_list(int ac, const char *av[], t_slct *elem, int *tot)
{
	t_slct			*tmp;
	t_slct			*new;

	tmp = elem;
	while (*tot + 1 < ac)
	{
		new = (t_slct*)malloc(sizeof(t_slct));
		new->name = ft_strdup_const(av[*tot + 1]);
		new->ind = *tot + 1;
		if (elem == NULL)
		{
			new->underl = true;
			elem = new;
		}
		else
		{
			tmp->next = new;
			new->prev = tmp;
		}
		tmp = new;
		++*tot;
	}
	new->next = elem;
	elem->prev = new;
	return (elem);
}
