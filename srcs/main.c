/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/15 15:20:16 by cumberto          #+#    #+#             */
/*   Updated: 2017/09/26 14:03:27 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

static void			program_int(int sig)
{
	if (sig == SIGINT || sig == SIGQUIT)
	{
		tputs(tgetstr("cl", NULL), 1, putsomechar);
		restore();
		exit(1);
	}
	tputs(tgetstr("ve", NULL), 1, putsomechar);
	unset_attr();
	if (sig == SIGTSTP)
	{
		signal(SIGTSTP, SIG_DFL);
		ioctl(0, TIOCSTI, "\032");
	}
	if (sig == SIGTERM)
		exit(1);
}

static void			print_update(int sig)
{
	t_slct			*tmp;

	tmp = g_data.curr;
	if (sig == SIGCONT)
	{
		tputs(tgetstr("vi", NULL), 1, putsomechar);
		signal(SIGTSTP, program_int);
		set_attr();
		fill_data(&g_data);
	}
	else if (sig == SIGWINCH)
		fill_data(&g_data);
	if (g_data.tot < (g_data.win[ROW] * (g_data.win[COL] / g_data.max)))
		g_data.first = g_data.list;
	if (tmp->pos[COL] + g_data.max > g_data.win[COL])
	{
		while (tmp->pos[ROW] != 0 || tmp->pos[COL] != g_data.max)
			tmp = tmp->prev;
		g_data.first = tmp;
	}
	print_list(&g_data, 0, 0);
}

void				restore(void)
{
	if (g_data.tot > 0)
		free_list(g_data.list, g_data.tot);
	tputs(tgetstr("ve", NULL), 1, putsomechar);
	unset_attr();
}

static void			set_signals(void)
{
	set_attr();
	signal(SIGWINCH, print_update);
	signal(SIGCONT, print_update);
	signal(SIGINT, program_int);
	signal(SIGQUIT, program_int);
	signal(SIGTSTP, program_int);
	signal(SIGKILL, program_int);
	signal(SIGTERM, program_int);
	tputs(tgetstr("vi", NULL), 1, putsomechar);
}

int					main(int ac, const char *av[])
{
	int				buf;
	t_data			elem;

	if (ac < 2)
		return (1);
	if (tgetent(NULL, getenv("TERM")) > 0)
	{
		set_signals();
		elem.tot = 0;
		elem.list = make_list(ac, av, NULL, &elem.tot);
		fill_data(&elem);
		elem.first = elem.list;
		elem.curr = elem.list;
		ft_memcpy(&g_data, &elem, sizeof(t_data));
		print_list(&elem, 0, 0);
		buf = 0;
		while (buf != 10 && elem.tot > 0)
			key_reader(&elem, &buf);
		final_print(elem);
		restore();
	}
	else
		ft_dprintf(2, "TERM not set\n");
	return (0);
}
