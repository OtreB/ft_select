/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   term_attr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/16 12:12:30 by cumberto          #+#    #+#             */
/*   Updated: 2017/09/25 14:30:23 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

void				set_attr(void)
{
	struct termios	attr;

	tcgetattr(0, &attr);
	attr.c_lflag &= ~ICANON;
	attr.c_lflag &= ~ECHO;
	attr.c_cc[VMIN] = 1;
	attr.c_cc[VTIME] = 0;
	tcsetattr(0, TCSANOW, &attr);
}

void				unset_attr(void)
{
	struct termios	attr;

	tcgetattr(0, &attr);
	attr.c_lflag |= ICANON;
	attr.c_lflag |= ECHO;
	tcsetattr(0, TCSANOW, &attr);
}
