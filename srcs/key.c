/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/15 15:20:16 by cumberto          #+#    #+#             */
/*   Updated: 2017/09/26 14:02:08 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

static t_slct		*delete_elem(t_data *data)
{
	t_slct			*prev;
	t_slct			*next;
	t_slct			*curr;

	curr = data->curr;
	prev = curr->prev;
	next = curr->next;
	if (data->list == curr)
		data->list = curr->next;
	if (data->first == curr)
		data->first = curr->next;
	prev->next = next;
	next->prev = prev;
	next->underl = true;
	data->tot = data->tot - 1;
	fill_data(data);
	if (curr->ind != data->tot + 1)
		update_ind(next, data->tot);
	free_list(curr, 1);
	print_list(data, 0, 0);
	return (next);
}

static void			check_col_change(t_data *data, t_slct *elem, int buf)
{
	t_slct			*tmp;

	tmp = elem;
	if (buf == RIGHT || buf == DOWN || buf == 32)
		while (tmp->pos[ROW] != 0 || tmp->pos[COL] != data->max)
			tmp = tmp->prev;
	else if (buf == LEFT || buf == UP)
		while (tmp->pos[ROW] != 0)
			tmp = tmp->prev;
	data->first = tmp;
	print_list(data, 0, 0);
}

static t_slct		*arrow_key(t_data *elem, int buf, int cnt)
{
	int				num;
	t_slct			*tmp;

	num = cnt_size(elem, buf);
	tmp = elem->curr;
	tmp->underl = false;
	print_line(tmp);
	if (buf == UP)
		tmp = tmp->prev;
	else if (buf == DOWN)
		tmp = tmp->next;
	else
		while (cnt < num)
		{
			if (buf == RIGHT)
				tmp = tmp->next;
			else
				tmp = tmp->prev;
			cnt++;
		}
	if (tmp->pos[COL] + elem->max >= elem->win[COL])
		check_col_change(elem, tmp, buf);
	tmp->underl = true;
	print_line(tmp);
	return (tmp);
}

static t_slct		*space(t_data *elem)
{
	t_slct			*tmp;

	tmp = elem->curr;
	tmp->rev = tmp->rev ? false : true;
	tmp->underl = false;
	print_line(tmp);
	tmp = tmp->next;
	tmp->underl = true;
	print_line(tmp);
	if (tmp->pos[COL] + elem->max >= elem->win[COL])
		check_col_change(elem, tmp, 32);
	return (tmp);
}

t_slct				*key_reader(t_data *elem, int *ret)
{
	int				buf;
	t_slct			*tmp;

	*ret = 0;
	read(0, ret, 8);
	update_data(elem);
	tmp = elem->curr;
	buf = *ret;
	if (buf == 27)
	{
		tputs(tgetstr("cl", NULL), 1, putsomechar);
		restore();
		exit(0);
	}
	else if (buf == 32)
		elem->curr = space(elem);
	else if (buf == UP || buf == DOWN || buf == LEFT || buf == RIGHT)
		elem->curr = arrow_key(elem, buf, 0);
	else if (buf == DELETE || buf == CANC)
		elem->curr = delete_elem(elem);
	g_data.curr = elem->curr;
	g_data.first = elem->first;
	g_data.list = elem->list;
	g_data.tot = elem->tot;
	return (tmp);
}
