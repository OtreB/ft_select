/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/15 15:20:16 by cumberto          #+#    #+#             */
/*   Updated: 2017/09/25 14:33:13 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

int				putsomechar(int ch)
{
	write(0, &ch, 1);
	return (0);
}

void			final_print(t_data elem)
{
	int			tot;
	t_slct		*tmp;

	tot = elem.tot;
	tmp = elem.list;
	tputs(tgetstr("cl", NULL), 1, putsomechar);
	while (tot)
	{
		if (tmp->rev == true)
		{
			ft_printf("%s", tmp->name);
			write(1, " ", 1);
		}
		tmp = tmp->next;
		--tot;
	}
}

int				len_finder(t_slct *elem, unsigned int tot)
{
	t_slct		*tmp;
	int			len;
	int			max;

	max = 0;
	tmp = elem;
	while (tot > 0)
	{
		if ((len = ft_strlen(tmp->name)) > max)
			max = len;
		tmp = tmp->next;
		--tot;
	}
	return (max);
}

void			print_line(t_slct *elem)
{
	tputs(tgoto(tgetstr("cm", NULL), elem->pos[COL], elem->pos[ROW]),
			1, putsomechar);
	if (elem->underl == true)
		tputs(tgetstr("us", NULL), 1, putsomechar);
	if (elem->rev)
		tputs(tgetstr("mr", NULL), 1, putsomechar);
	ft_dprintf(0, "%s", elem->name);
	if (elem->underl == true || elem->rev == true)
		tputs(tgetstr("me", NULL), 1, putsomechar);
}

void			print_list(t_data *elem, int row, int col)
{
	int			cnt;
	t_slct		*tmp;

	tmp = elem->first;
	cnt = 0;
	tputs(tgetstr("cl", NULL), 1, putsomechar);
	if (elem->win[COL] <= elem->max)
		ft_dprintf(0, "size too small\n");
	while (cnt < elem->tot)
	{
		while (row < elem->win[ROW] && cnt < elem->tot)
		{
			tmp->pos[ROW] = row;
			tmp->pos[COL] = col;
			if (col + elem->max < elem->win[COL])
				print_line(tmp);
			cnt++;
			row++;
			tmp = tmp->next;
			if (tmp->prev->ind == elem->tot)
				break ;
		}
		row = 0;
		col += elem->max;
	}
}
