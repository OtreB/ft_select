# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: cumberto <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/09/15 15:11:56 by cumberto          #+#    #+#              #
#    Updated: 2017/09/25 14:27:56 by cumberto         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = ft_select

SRCS_NAME = main.c \
			term_attr.c \
			print.c \
			make_list.c \
			key.c \
			util.c \

SRCS_PATH = srcs

SRCS = $(addprefix $(SRCS_PATH)/,$(SRCS_NAME))

OBJ_NAME = $(SRCS_NAME:.c=.o)

OBJ_PATH = obj

OBJ = $(addprefix $(OBJ_PATH)/,$(OBJ_NAME))

INCLUDES = -Iincludes

CFLAGS = -Werror -Wall -Wextra

all: $(NAME)

$(NAME): libft/libft.a $(OBJ)
	gcc $(CFLAGS) $(INCLUDES) $(OBJ) libft/libft.a -lncurses -o $(NAME)

libft/libft.a:
	make -C ./libft

$(OBJ_PATH)/%.o:$(SRCS_PATH)/%.c
	mkdir -p $(OBJ_PATH)
	gcc $(CFLAGS) $(INCLUDES) -c $< -o $@

clean:
	make -C ./libft clean
	rm -Rf $(OBJ_PATH)

fclean: clean
	make -C ./libft fclean
	rm $(NAME)

re: fclean all

test:
	gcc -g $(CFLAGS) $(INCLUDES) $(SRCS) libft/libft.a -lncurses -o $(NAME)
