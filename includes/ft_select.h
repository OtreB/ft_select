/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_select.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cumberto <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/15 15:09:46 by cumberto          #+#    #+#             */
/*   Updated: 2017/09/25 16:26:58 by cumberto         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_SELECT_H
# define FT_SELECT_H

# include <unistd.h>
# include <termios.h>
# include <term.h>
# include <curses.h>
# include <sys/ioctl.h>
# include "../libft/includes/libft.h"
# include <signal.h>

# define ROW 0
# define COL 1
# define UP 4283163
# define DOWN 4348699
# define LEFT 4479771
# define RIGHT 4414235
# define DELETE 127
# define CANC 2117294875

typedef struct		s_slct
{
	char			*name;
	bool			rev;
	bool			underl;
	int				pos[2];
	int				ind;
	struct s_slct	*next;
	struct s_slct	*prev;
}					t_slct;

typedef struct		s_data
{
	t_slct			*list;
	t_slct			*first;
	t_slct			*curr;
	int				tot;
	int				max;
	int				win[2];
	int				lcol;
}					t_data;

t_data		g_data;

void				set_attr(void);
void				unset_attr(void);
t_slct				*make_list(int ac, const char *av[], t_slct *elem,
		int *tot);
void				print_list(t_data *elem, int row, int col);
void				final_print(t_data elem);
void				print_line(t_slct *elem);
int					putsomechar(int ch);
t_slct				*key_reader(t_data *elem, int *buf);
void				free_list(t_slct *elem, unsigned int tot);
void				restore();
int					len_finder(t_slct *elem, unsigned int tot);
void				fill_data(t_data *elem);
void				update_ind(t_slct *elem, int tot);
int					cnt_size(t_data *elem, int buf);
void				update_data(t_data *elem);

#endif
